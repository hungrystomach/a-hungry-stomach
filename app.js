(function (angular) {
    'use strict';

    angular.module('app', [
        'ui.router',
        'ngMaterial'
    ]);

})(window.angular);