/**
 * Created by XUZH0001 on 7/7/2017.
 */

(function (angular) {
    'use strict';

    angular.module('app')
        .service("firebaseService", firebaseService);

    firebaseService.$inject = [];

    function firebaseService() {
        var dbRoot = firebase.database().ref();
        return {
            root: dbRoot,
            storage: firebase.storage().ref(),
            list: dbRoot.child('list'),
            basket: dbRoot.child('basket'),
            messages: dbRoot.child('messages'),
            atomicUpdate: atomicUpdate
        };

        function atomicUpdate(updates) {
            return dbRoot.update(updates);
        }
    }
})(window.angular);
