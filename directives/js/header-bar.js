angular
    .module('app')
    .directive('headerBar', headerBar);

function headerBar() {
    return {
        restrict: 'E',
        templateUrl: 'directives/html/header-bar.html',
        scope: {

        },
        controller: HeaderBarController,
        controllerAs: 'vm',
        bindToController: true
    };
}

HeaderBarController.$inject = ['$scope'];

function HeaderBarController($scope) {
    var vm = this;
    vm.testString = 'zhi';
}