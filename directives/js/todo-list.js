angular
    .module('app')
    .directive('todoList', todoList);

function todoList() {
    return {
        restrict: 'E',
        templateUrl: 'directives/html/todo-list.html',
        scope: {

        },
        controller: TodoListController,
        controllerAs: 'vm',
        bindToController: true
    };
}

TodoListController.$inject = ['$scope'];

function TodoListController($scope) {
    var vm = this;
    vm.todoItems = ['miao', 'wang', 'zhi'];
    vm.newItem = null;

    vm.addTodoItem = function(newItem) {
        vm.todoItems.push(newItem);
        vm.newItem = null;
    };
}