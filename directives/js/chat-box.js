angular
    .module('app')
    .directive('chatBox', chatBox);

function chatBox() {
    return {
        restrict: 'E',
        templateUrl: 'directives/html/chat-box.html',
        scope: {

        },
        controller: ChatBoxController,
        controllerAs: 'vm',
        bindToController: true
    };
}

ChatBoxController.$inject = ['$scope'];

function ChatBoxController($scope) {
    var vm = this;
    vm.testString = 'wang';
}