(function (angular) {
    'use strict';

    angular
        .module('app')
        .config(Config);

    Config.$inject = ['$stateProvider', '$urlRouterProvider'];

    function Config($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('404', {
                url: '/404',
                templateUrl: 'app/views/404.html'
            })
            .state('todo', {
                url: '/todo',
                templateUrl: 'views/todo.html'
            })
            .state('chat', {
                url: '/chat',
                templateUrl: 'views/chat.html'
            });

        $urlRouterProvider.otherwise("/todo");

    }
})(window.angular);
